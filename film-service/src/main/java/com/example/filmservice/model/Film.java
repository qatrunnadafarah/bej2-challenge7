package com.example.filmservice.model;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "film")
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "film_id")
    private Integer filmID;

    @Column(name = "title")
    private String title;

    @Column(name = "schedule")
    private String schedule;

    @Column(name = "status")
    private Boolean sedangTayang;
}
