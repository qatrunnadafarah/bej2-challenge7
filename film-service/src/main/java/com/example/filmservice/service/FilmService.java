package com.example.filmservice.service;

import com.example.filmservice.model.Film;
import com.example.filmservice.model.response.FilmResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {

    void addFilm (Film film) throws Exception;

    void updateFilm(Integer filmID) throws Exception;

    void deleteFilm(Integer film);

    List<Film> getAllFilms();

    List<FilmResponse> getScheduleByFilmId(String filmId);

    Film searchFilmBySchedule(Integer schedule);

    Film searchFilmByID(Integer filmID);

    Film searchFilmByTitle(String title);
}
