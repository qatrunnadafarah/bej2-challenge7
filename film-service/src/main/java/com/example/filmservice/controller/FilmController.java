package com.example.filmservice.controller;

import com.example.filmservice.model.Film;
import com.example.filmservice.model.request.FilmRequest;
import com.example.filmservice.model.response.FilmResponse;
import com.example.filmservice.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/film")
public class FilmController {

    @Autowired
    FilmService filmService;

    //Menambahkan film baru
    @PostMapping("/add")
    public ResponseEntity addFilm(@RequestBody FilmRequest filmRequest) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "Film Added");

        try {
            Film film = new Film();
            film.setTitle(filmRequest.getTitle());
            film.setSchedule(filmRequest.getSchedule());
            film.setSedangTayang(filmRequest.getSedangTayang());
            return new ResponseEntity(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "Adding film failed!." + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.BAD_GATEWAY);
        }
    }

    //Mengupdate film
    @PutMapping(value = "/{filmID}")
    public ResponseEntity updateFilm(@RequestBody FilmRequest filmRequest,
                                     @PathVariable("filmID") Integer filmID) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "Film Updated");

        try {
            String title = filmRequest.getTitle();
            filmService.updateFilm(filmID);
            return new ResponseEntity(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "Updating film failed!." + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.BAD_GATEWAY);
        }
    }

    //Menghapus film
    @DeleteMapping(value = "/{filmID}")
    public ResponseEntity deleteFilm(@PathVariable("filmID") Integer filmID) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "Film Deleted");

        try {
            filmService.deleteFilm(filmID);
            return new ResponseEntity(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "Deleting film failed!." + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.BAD_GATEWAY);
        }
    }

    //Menampilkan film yang sedang tayang
    @GetMapping("/{schedule}")
    public ResponseEntity searchFilmBySchedule(@PathVariable("schedule") Integer schedule) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "Get Film by schedule Success");

        try {
            Film film = filmService.searchFilmBySchedule(schedule);
            FilmResponse response = new FilmResponse(film.getSchedule(), film.getTitle(), film.getSedangTayang());
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            resp.put("message", "Get Film by schedule failed!." + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.BAD_GATEWAY);
        }
    }

    //Menampilkan jadwal dari film tertentu
    @GetMapping("/{filmID}")
    public ResponseEntity searchFilmByID(@PathVariable("filmID") String filmID) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "Search Film by ID Success");

        try {
            List<FilmResponse> filmResponses = filmService.getScheduleByFilmId(filmID);
            return new ResponseEntity(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "Get Film by ID failed!." + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.BAD_GATEWAY);
        }
    }
}
