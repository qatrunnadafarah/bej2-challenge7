package com.example.filmservice.scheduler;

import com.example.filmservice.service.FilmService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.quartz.Job;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.List;

@Component
public class FilmScheduler implements Job {

    @Autowired
    public FilmService filmService;
    @Scheduled(cron = "0 0 0 ? * *")  // in one Day

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String todayDate = String.valueOf(new SimpleDateFormat("yyyy-MM-dd"));
        List<Integer> films = (List<Integer>) filmService.searchFilmBySchedule(Integer.valueOf(todayDate));

        for (int i = 0; i < films.size(); i++) {
            try {
                filmService.updateFilm(films.get(i));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}

