package com.example.userservice.service;

import com.example.userservice.model.UserActive;
import com.example.userservice.repository.UserActiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserActiveRepository userActiveRepository;

    @Override
    public void addUser(UserActive userActive) {
        userActiveRepository.save(userActive);
    }

    @Override
    public void updateUser(String username, String email, String password) throws Exception {

    }

    @Override
    public void deleteUserById(Integer userActive) throws Exception {
        userActiveRepository.deleteById(userActive);
    }
}
