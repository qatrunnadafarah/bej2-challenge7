package com.example.userservice.model;

import com.example.userservice.model.enumeration.Roles;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer RoleId;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Roles name;
}
