package com.example.userservice.model.enumeration;

public enum Roles {

    CUSTOMER, ADMIN, SELLER
}
