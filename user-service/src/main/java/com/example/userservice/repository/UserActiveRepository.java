package com.example.userservice.repository;

import com.example.userservice.model.UserActive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository //langsung dibikin bean cmn blm terhubung. pakai autowired ke repositorytest
public interface UserActiveRepository extends JpaRepository<UserActive, Integer> { //bikin model ke tabel mana, PK


}
